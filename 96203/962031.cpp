#include <iostream>
using namespace std ;
class Node {
 public:
	int a ;
	Node *next ;
	Node() {
	   this->a = 0 ;
	   this->next = 0 ;
	}
	~Node() {
		delete next ;
	}
	friend istream &operator>> (istream &is , Node &a) ;	
	friend ostream &operator<< (ostream &os , Node &a) ;
};
ostream &operator<< (ostream &os , Node &a) {
	os << "a =  " << a.a << endl ;
	return os ;
}
istream &operator>> (istream &is , Node &a) {
	cout << "enter the a :  " ;
	is >> a.a ;
	return is;
}
class link_list {
 public:
	Node *head ;
	link_list (){
		head = 0 ;
	}
	~link_list (){
		Node *pn=head;
  		int i;
  		for ( i=0;pn;i++,pn=pn->next);
      	for (;i>0;i--){
  		 for (pn=head;pn->next;pn=pn->next) delete pn;
  	}
	}
	friend link_list operator+ (const link_list &a , const link_list &b) ;
	friend ostream &operator<< (ostream &os , link_list &a) ;
	bool sort(void) ;
	Node& operator[] (int a) ;
	void append(void) ;
	void show(void) ;
	void remove(void) ;
	int length (void) ;
};
bool link_list::sort(void) {
	Node *pl = head ;
	bool ret = false ;
	if (head) {
		for ( ; pl ; pl=pl->next) {
			for (Node *pl1 = pl ; pl1 ; pl1 = pl1->next) {
				if (pl1->a < pl->a) {
					int a ;
					a = pl->a ;
					pl->a = pl1->a ;
					pl1->a = a ;
					ret = true ;
				}
			}
		}
	}else {
		return false ;
	}
	return ret ;
}
link_list operator+ (const link_list &a , const link_list &b) {
	Node *pl1 = a.head , *pl2 = b.head , *sum = new Node ;
	link_list s ;
	s.head = sum ;
	for ( ; pl1 ; pl2 = pl2->next ) {
		sum->a = pl1->a + pl2->a ;
		pl1 = pl1->next ;
		if (pl1) {
			sum->next = new Node ;
			sum = sum->next ;	
		}
	}
	sum->next = 0 ;
	return s ;
}
ostream &operator<< (ostream &os , link_list &a) {
	Node *pl = a.head ;
	for (; pl ; pl=pl->next) {
		os << "a =  " << pl->a << endl ;
	}
	return os ;
}
Node& link_list::operator[] (int a) {
	Node *pl = head , *temp ;
	cout << length() << endl ;
	if (head)
	   if(length()-1 >= a){
			for ( int i = 0 ; i < a ;  i++ , pl=pl->next) ;
			temp = pl ;
	   }else {
	   	cout << "this is out of range !!\n" ;
	   	throw 2 ;
	   }
	else {
		cout << "this is empty list !! \n" ;
		throw 2 ;
	}
}
int link_list::length (void) {
	Node *pl = head ;
	int i = 0;
	if(head) {
		for (; pl ; pl=pl->next)
			i++ ;
	}else {
		cout << "list is empty!!\n" ;
	}
	return i ;
}
void link_list::remove(void) {
	Node *pl = head , *pv ;
	if (head) {
		if (head->next) {
			for ( ; pl->next ; pl = pl->next)
				pv = pl ;
			pv->next = 0 ;
			delete pl ;
		}else {
			cout << "this list is not enough to remove ! \n" ;
		}
	}else {
		cout << "this list is empty!\n" ;
	}
	
}
void link_list::append(void) {
	Node *pl = head , *temp ;
	temp = new Node ;
	cin >> *temp ;
	if(head) {
		for ( ; pl->next ; pl = pl->next) ;
		pl->next = temp ;
		pl = pl->next ;
	}else {
		pl = head = temp ;
	}
	pl->next = 0 ;
}
void link_list::show(void) {
	if (head) {
		Node *pl = head ;
		for ( ; pl ; pl = pl->next) {
			cout << *pl ;
		}
	}else {
		cout << "this is empty !! \n" ;
	}
}
int main () {
	link_list a , b , c , d;
	a.append() ; a.append() ; a.append() ; a.append() ;
	cout << a ;
	cout << "---------------\n" ;
	a.sort() ;
	cout << a ;
	b.append() ; b.append() ; b.append() ; b.append() ;
	b.show();
	d.append() ; d.append() ; d.append() ; d.append() ;
	c = a + b + d ;
	c.show() ;
	cout << "----------------\n" ;
	cout << c ;
	cout << a.length() << endl ;
	try{
		cout << a[4] ;	
	}catch (...){
	}
	a.show() ;
	a.remove() ;
	a.show() ;
	a.remove() ;
	a.show() ;
}
