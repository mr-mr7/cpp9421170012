#include <iostream>
#include <fstream>
using namespace std ;

bool mo(const char[15] , const char[15]) ;
void strc (char[15] , const char[15]) ;

class date {
	public:
	int day , month , year ;
	public:
	date (int day=0 , int month=0 , int year=0): day(day) , month(month) , year(year){} ;

};
//START Class student
class student:public date {
	public:
	char stdno[15] , fname[15] , lname[15] ;
	friend istream& operator>> (istream&is , student&a) ;
	friend ostream& operator<< (ostream&os , const student&a) ;
	char* operator[](char*);
	void add_stds (void) ;
	void disp_crss_std (void) ;
	void disp_tchs_std (void) ;
	void dis_avg_std (void) ;
};

char* student::operator[](char a[15]) {
	ifstream stu1("student.b",ios::binary) ;
	if(!stu1 ){cout << "this file cannot open!!!\n" ; exit(0) ;	}
	stu1.read((char*) &(*this ), (sizeof (*this))) ;
	for ( ; !stu1.eof() ; ) {
		if (mo(this->stdno,a)) {
			return this->fname ;
		}
		stu1.read((char*) &(*this ), (sizeof (*this))) ;
	}
	stu1.close() ;
	return 0 ;	
}

istream& operator>> (istream&is , student&a) {
	cout << "enter frist name :  " ;
	is >> a.fname ;
	cout << "enter last name :  " ;
	is >> a.lname ;
	cout << "enter student number :  " ;
	is >> a.stdno ;
	cout << "enter brith day :  " ;
	is >> a.day ;
	cout << "enter brith month :  " ;
	is >> a.month ;
	cout << "enter brith year :  " ;
	is >> a.year ;
	return is ;
}

ostream& operator<< (ostream&os , const student&a)  {
		os << "frist name :  " << a.fname << endl ;
		os << "last name :  " << a.lname << endl ;
		os << "student number :  " << a.stdno << endl ;
		os << a.year << " / " << a.month << " / " << a.day << endl ;
		cout << "---------\n" ;
	return os ;
}


//END Class student

//START Class teacher
class teacher:public date {
	public:
	int tchno ;
	char fname[15] , lname[15] ;
	friend istream& operator>> (istream&is , teacher&a) ; ;
	friend ostream& operator<< (ostream& os , const teacher&a) ;
	char* operator[] (int);
	void add_tchs (void) ;
	void avg_crs_tch (void) ;
	void avg_crss_tch (void) ;
	void disp_stds_tch (void) ;
	void disp_crss_tch (void) ;
};

char* teacher::operator[] (int a) {
	ifstream tch ("teacher.b",ios::binary) ;
	if (!tch) {cout << "this file cannot open !!!\n"; exit(0);}
	tch.read((char*) &(*this) , sizeof(*this));
	for ( ; !tch.eof() ; ) {
		if (this->tchno==a) {
			return this->fname ;
		}
		tch.read((char*) &(*this) , sizeof(*this));
	}
	tch.close() ;
	return 0;
}

istream& operator>> (istream& is , teacher&a) {
	cout << "enter frist name :  " ;
	is >> a.fname ;
	cout << "enter last name :  " ;
	is >> a.lname ;
	cout << "enter teacher number :  " ;
	is >> a.tchno ;
	cout << "enter day of employment :  " ;
	is >> a.day ;
	cout << "enter month of employment:  " ;
	is >> a.month ;
	cout << "enter year of employment :  " ;
	is >> a.year ;
	return is ;
}

ostream& operator<< (ostream& os , const teacher&a)  {
		os << "frist name :  " << a.fname << endl ;
		os << "last name :  " << a.lname << endl ;
		os << "teacher number :  " << a.tchno << endl ;
		os << a.year << " / " << a.month << " / " << a.day << endl ;
		cout << "--------\n" ;
	return os;
}

//END Class teacher

//START Class course
class course {
	public:
	int csno ;
	char cname[15] ;
	friend istream& operator>> (istream& is , course&a) ;
	friend ostream& operator<< (ostream& os , const course&a) ;
	char* operator[] (int) ;
	course (int csno=0):csno(csno) {};
	void add_cors (void) ;
	void disp_stds_crs (void) ;
};

char* course::operator[] (int a) {
	ifstream crs("course.b",ios::binary) ;
	if(!crs) {cout << "this file cannot to open!!!\n"; exit(0);	}
	crs.read((char*)&(*this) , sizeof((*this)));
	for ( ; !crs.eof() ; ) {
		if (this->csno==a) {
			return this->cname ;
		}
		crs.read((char*)&(*this) , sizeof((*this))) ;
	}
	return 0 ;
}

istream& operator>> (istream& is , course&a) {
	cout << "enter name course :  " ;
	is >> a.cname ;
	cout << "enter course number :  " ;
	is >> a.csno ;
	return is ;
}

ostream& operator<< (ostream& os , const course&a)  {
		os << "course name :  " << a.cname << endl ;
		os << "course number :  " << a.csno << endl ;
		cout << "--------\n" ;
	return os;
}
//End Class course


//START Class teacher and course
class tch_crs {
	public:
	int csno , tchno ;
	tch_crs (int csno=0 , int tchno=0):csno(csno) , tchno(tchno){};
	friend ostream& operator<< (ostream &os , const tch_crs&a);
	bool operator==(const tch_crs&a);
	void add_tchs_crss (void) ;
	
};

bool tch_crs::operator==(const tch_crs&a) {
	if (this->csno==a.csno && this->tchno==a.tchno)
		return true ;
	return false ;
}

ostream& operator<< (ostream& os , const tch_crs&a)  {
	os << "course number :  " << a.csno << endl ;
	os << "teacher number :  " << a.tchno << endl ;
	cout << "-------\n" ;
	return os;
}

//END Class teacher and course


//START Class student and course
class std_crs {
	public:
	char stdno[15] ;
	int csno ;
	int tchno ;
	double score ;
	std_crs (double score = 115 , int csno=0 ,int tchno=0):score(score),csno(csno),tchno(tchno) {} ;
	friend ostream& operator<< (ostream &os , const std_crs&a);
	void add_stds_crss (void) ;
	void rem_stds_crss (void) ;
	bool operator==(const std_crs&a) ;
};

ostream& operator<< (ostream& os , const std_crs&a)  {
		os << "student number :  " << a.stdno << endl ;
		os << "course number :  " << a.csno << endl ;
		os << "teacher number :  " << a.tchno << endl ;
		os << " score :  " << a.score << endl ;
		cout << "-----\n";
	return os;
}

bool std_crs::operator==(const std_crs&a) {
	if (this->csno==a.csno && mo(this->stdno , a.stdno))
		return true ;
	return false ;
}
//END Class student and course


//------------------- START main ----------------------

int main() {
student a , aa ;
teacher b , bb ;
course c , cc ;
tch_crs d , dd ;
std_crs e , ee ;
int choose , std1=1 , tch1=1 , crs1=1 , tch_crs1=1 , std_crs1=1;


cout << "\n		*  *  *  *  *  *  *  *  *  *\n" ;
cout << "		*  *  *  M  E  N  O  *  *  *\n" ;
cout << "		*  *  *  *  *  *  *  *  *  *\n\n" ;
cout << "^_^ Pls Choose one of the following options by typing the number on each one ^_^ \n\n" ;
cout << "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n" ;
cout << "# add student (1) " << endl ;
cout << "# display all student (100) " << endl ;
cout << "# add teacher (2) " << endl ;
cout << "# display all teacher (200) " << endl ;
cout << "# add course  (3) " << endl ;
cout << "# display all course  (300) " << endl ;
cout << "# putting course to the teacher           (4) " << endl ;
cout << "# display all teacher by course           (400) " << endl ;
cout << "# putting course to the student           (5) " << endl ;
cout << "# display all student by course           (500) " << endl ;
cout << "# Display student information of corse    (6) " << endl ;
cout << "# Display student's lessons               (7) " << endl ;
cout << "# Display Student Teachers                (8) " << endl ;
cout << "# Display courses of teacher              (9) " << endl ;
cout << "# Display student Average                 (10) " << endl ;
cout << "# Display average teacher's course        (11) " << endl ;
cout << "# Display average marks of a teacher      (12) " << endl ;
cout << "# Display Students of a Teacher           (13) " << endl ;
cout << "# remove course to student                (14) " << endl ;
cout << "*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*\n\n" ;

//------------ START check to open main file ----------------
ifstream std2("student.b",ios::binary) ;
if (!std2) {std1=0;}
ifstream tch2("teacher.b",ios::binary) ;
if (!tch2) {tch1=0;}
ifstream  crs2("course.b",ios::binary) ;
if (!crs2) {crs1=0;}
ifstream std_crs2("std_crs.b",ios::binary) ;
if (!std_crs2) {std_crs1=0;}
ifstream tch_crs2("tch_crs.b",ios::binary) ;
if (!tch_crs2) {tch_crs1=0;}
//------------ END check to open main file ----------------


cout << "Pls Choose one of the above :  " ;
cin >> choose ;
cout << "*--*--*--*\n" ;


if (choose==1) {
	a.add_stds() ;
}
if (choose==2) {
	b.add_tchs() ;
}
if (choose==3) {
	c.add_cors() ;
}
if (choose==4) {
	if (crs1==0 ) {
		cout << "file 'course.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> crs1 ;
		if (crs1==1) {
			c.add_cors () ;
		}
	}
	if (tch1==0 ) {
		cout << "file 'teacher.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add teacher ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> tch1 ;
		if (tch1==1) {
			b.add_tchs () ;
		}
	}
	d.add_tchs_crss() ;
}
if (choose==5) {
	if (std1==0 ) {
		cout << "file 'student.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std1 ;
		if (std1==1) {
			a.add_stds() ;
		}
	}
	if (crs1==0 ) {
		cout << "file 'course.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> crs1 ;
		if (crs1==1) {
			c.add_cors () ;
		}
	}
	e.add_stds_crss() ;
}
if (choose==6) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	c.disp_stds_crs() ;
}
if (choose==7) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	a.disp_crss_std() ;	
}
if (choose==8) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	a.disp_tchs_std() ;
}
if (choose==9) {
	if (tch_crs1==0) {
		cout << "file 'tch_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> tch_crs1 ;
		if (tch_crs1==1) {
			d.add_tchs_crss() ;
		}
	}
	b.disp_crss_tch() ;
}
if (choose==10) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	a.dis_avg_std() ;
}
if (choose==11) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	b.avg_crs_tch() ;
}
if (choose==12) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	b.avg_crss_tch() ;
}
if (choose==13) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	b.disp_stds_tch() ;
}
if (choose==14) {
	if (std_crs1==0) {
		cout << "file 'std_crs.b' notfind ! pls frist add student!!!\n" ;
		cout << "Do you like add student & course ? enter (1) otherwise Enter another number !!! \n" ;
		cin >> std_crs1 ;
		if (std_crs1==1) {
			e.add_stds_crss() ;
		}
	}
	e.rem_stds_crss() ;
}
if (choose==100) {
	cout << "---------- list_student -----------\n" ;
	ifstream std("student.b",ios::binary) ;
	if (!std){cout << "this file cannot open!!!\n"; exit(0);}
	std.read((char*)&aa , sizeof(aa));
	for (; !std.eof() ;) {
		cout << aa ;
		std.read((char*)&aa , sizeof(aa));
	}
	std.close() ;
}
if (choose==200) {
	cout << "---------- list teacher -----------\n" ;
	ifstream tch("teacher.b",ios::binary) ;
	if (!tch){cout << "this file cannot open!!!\n"; exit(0);}
	tch.read((char*)&bb , sizeof(bb));
	for (; !tch.eof() ;) {
		cout << bb ;
		tch.read((char*)&bb , sizeof(bb));
	}
	tch.close() ;
}

if (choose==300) {
	cout << "---------- list course -----------\n" ;
	ifstream crs("course.b",ios::binary) ;
	if (!crs){cout << "this file cannot open!!!\n"; exit(0);}
	crs.read((char*)&cc , sizeof(cc));
	for (; !crs.eof() ;) {
		cout << cc ;
		crs.read((char*)&cc , sizeof(cc));
	}
	crs.close() ;
}
if (choose==400) {
	cout << "---------- list course and teacher -----------\n" ;
	ifstream tch_crs1("tch_crs.b",ios::binary) ;
	if (!tch_crs1){cout << "this file cannot open!!!\n"; exit(0);}
	tch_crs1.read((char*)&dd , sizeof(dd));
	for (; !tch_crs1.eof() ;) {
		cout << dd ;
		tch_crs1.read((char*)&dd , sizeof(dd));
	}
	tch_crs1.close() ;
}
if (choose==500) {
	cout << "---------- list course and student -----------\n" ;
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if (!std_crs1){cout << "this file cannot open!!!\n"; exit(0);}
	std_crs1.read((char*)&ee , sizeof(ee));
	for (; !std_crs1.eof() ;) {
		cout << ee ;
		std_crs1.read((char*)&ee , sizeof(ee));
	}
	std_crs1.close() ;
}
}

//------------------- END main ----------------------




void student::add_stds (void) {
	student a ;
	ofstream stu ("student.b",ios::binary | ios::app);
	if(!stu ){cout << "this file cannot open!!!\n" ; exit(0) ;	}
	int n ;
	cout << "enter the number of student to add :  " ;
	cin >> n ;
	for (; n>0 ; n--) {
		cin >> a ;
		stu.write((char*) &a , sizeof(a)) ;
	}
	stu.close();
}


void teacher::add_tchs (void) {
	teacher a ;
	ofstream tch ("teacher.b",ios::binary | ios::app) ;
	if (!tch) {cout << "this file cannot open !!!\n"; exit(0);}
	cout << "enter the number of teacher to add :  " ;
	int n ;
	cin >> n ;
	for (; n>0 ; n--) {
		cin >> a ;
		tch.write((char*) &a , sizeof(a)) ;
	}
	tch.close();
}


void course::add_cors (void) {
	course a ;
	ofstream crs("course.b",ios::binary | ios::app) ;
	if(!crs) {cout << "this file cannot to open!!!\n"; exit(0);	}
	cout << "enter the number of course to add :  " ;
	int n ;
	cin >> n ;
	for (; n>0 ; n--) {
		cin >> a ;
		crs.write((char*) &a , sizeof(a)) ;
	}
	crs.close() ;
}


void tch_crs::add_tchs_crss (void) {
	course c1 ;
	teacher t1 ;
	tch_crs temp , temp2[610] ;
	int tchno[200] ;
	int csno[400] ;
	int n , i , m , at=0 , ac=0 , atc=0;
	ifstream crs1("course.b",ios::binary) ;
	if(!crs1){cout << "this file cannot to open !!!\n" ;  exit(0);	}
	ifstream tch1("teacher.b",ios::binary) ;
	if (!tch1) {cout << "this file cannot to open !!!\n" ; exit(0) ;	}
	crs1.read((char*) &c1 , sizeof(c1)) ;
	for (i=0 ; !crs1.eof() ; i++ , ac++) {
		csno[i] = c1.csno ;
		crs1.read((char*) &c1 , sizeof(c1)) ;
	}
	csno[i] = NULL ;
	tch1.read((char*) &t1 , sizeof(t1)) ;
	for (i=0 ; !tch1.eof() ; i++ , at++) {
		tchno[i] = t1.tchno ;
		tch1.read((char*) &t1 , sizeof(t1)) ;
	}
	tchno[i] = NULL ;
	tch1.close();
	crs1.close();

	ifstream tch_crs2("tch_crs.b",ios::binary) ;
	if(!tch_crs2){cout << "this file 'tch_crs2' cannot to open !!!\n" ;	}
	else {
		tch_crs2.read((char*)&temp2[0] , sizeof(temp2[0])) ;
		for (i=1 ; !tch_crs2.eof() ; i++,atc++) {
			tch_crs2.read((char*)&temp2[i] , sizeof(temp2[i])) ;
		}
		temp2[i] = NULL ;
		tch_crs2.close() ;
	}
	ofstream tch_crs1("tch_crs.b",ios::binary | ios::app) ;
	if (!tch_crs1) {cout << "this file 'tch_crs1' cannot to open!!!\n "; exit(0) ;	}
	do {
		cout << "enter the number of course and teacher to add :  " ;
		cin >> n ;
		if (n>=(at*ac)-atc) {
			cout << "this number is long !!! pls enter another number!!!" ;
		}
	}while (n>=(at*ac)-atc) ;
	i = 0 ;
	for ( ; n>0 ;) {
        while(1) {
            cout << "enter teacher number :  " ;
            cin >> temp.tchno ;
            int j=0 ;
            for ( ; tchno[j] ; j++){
                if (tchno[j] == temp.tchno)
                    break;
            }
            if (tchno[j] == temp.tchno)
                break;
        }
        cout << "temo.tchno :  " << temp.tchno << endl ;
        while(1) {
            cout << "enter course number :  " ;
            cin >> temp.csno ;
            int j=0 ;
            for ( ; csno[j] ; j++){
                if (csno[j] == temp.csno)
                    break;
            }
            if (csno[j] == temp.csno)
                break;
        }
        cout << "temp.csno :  " << temp.csno << endl ;
        for (i=0,m=0; temp2[i].csno ; i++) {
        	if (temp2[i]==temp)
        		m=1 ;
		}
		if (m==0) {
			n-- ;
			tch_crs1.write((char*)&temp , sizeof(temp)) ;
		}else {
			cout << "This has already been registered!!! pls try agin!\n" ;
		}		
	}
	tch_crs1.close();
}


void std_crs::add_stds_crss (void) {
	student s1 ;
	tch_crs temp3[600] ;
	std_crs temp , temp2[2000] ;
	char stdno[200][15] ;
	int n , i , m ;
	ifstream std1("student.b",ios::binary) ;
	if (!std1) {cout << "this file cannot to open !!!\n" ; exit(0) ;	}
	ifstream tch_crs1("tch_crs.b",ios::binary) ;
	if (!tch_crs1) {cout << "this file cannot to open !!!\n" ; exit(0) ;	}
	tch_crs1.read((char*)&temp3[0] , sizeof(temp3[0]));
	for (i=1 ; !tch_crs1.eof() ; i++) {
		tch_crs1.read((char*)&temp3[i] , sizeof(temp3[i]));
	}
	temp3[i].csno=NULL ;
	std1.read((char*) &s1 , sizeof(s1)) ;
	for (i=0 ; !std1.eof() ; i++) {
		for(int j=0 ; s1.stdno[j] ; j++)
			stdno[i][j] = s1.stdno[j] ;
		std1.read((char*) &s1 , sizeof(s1)) ;
	}
	stdno[i][0] = NULL ;
	std1.close();
	ifstream std_crs2("std_crs.b",ios::binary) ;
	if(!std_crs2){cout << "this file 'std_crs2' cannot to open !!!\n" ;	}
	else {
		std_crs2.read((char*)&temp2[0] , sizeof(temp2[0])) ;
		for (i=1 ; !std_crs2.eof() ; i++) {
			std_crs2.read((char*)&temp2[i] , sizeof(temp2[i])) ;
		}
		temp2[i] = NULL ;
		std_crs2.close() ;
	}
	ofstream std_crs1("std_crs.b",ios::binary | ios::app) ;
	if (!std_crs1) {cout << "this file 'std_crs1' cannot to open!!!\n "; exit(0) ;	}
	cout << "enter the number of course and student to add :  " ;
	cin >> n ;
	i = 0 ;
	for ( ; n>0 ;) {
        while(1) {
            cout << "enter student number :  " ;
            cin >> temp.stdno ;
            int j=0 ;
            for ( ; stdno[j][0] ; j++){
                if (mo(stdno[j],temp.stdno)) {
                    break;
                }
            }
            if (mo(stdno[j],temp.stdno))
                break;
            else	
            	cout << "this student number not find !!! pls try agan \n" ;
        }
        while(1) {
            cout << "enter course number :  " ;
            cin >> temp.csno ;
            cout << "enter teacher number :  " ;
            cin >> temp.tchno ;
            int j=0 ;
            for ( ; temp3[j].csno ; j++){
                if (temp3[j].csno == temp.csno && temp3[j].tchno == temp.tchno)
                    break;
            }
            if (temp3[j].csno == temp.csno && temp3[j].tchno == temp.tchno)
                break;
            else 
            	cout << "this teacher and course not find !!! pls try again \n" ;
        }
        cout << "enter course score :   " ;
        cin >> temp.score ;
        for (i=0,m=0; temp2[i].csno ; i++) {
        	if (temp2[i]==temp)
        		m=1 ;
		}
		if (m==0) {
			n-- ;
			std_crs1.write((char*)&temp , sizeof(temp)) ;
		}else {
			cout << "This has already been registered!!! pls try agin!\n" ;
		}		
		cout << "***********\n" ;
	}
	std_crs1.close();
	
}


void std_crs::rem_stds_crss (void) {
	std_crs temp[2000] , temp2[200] , temp3 ;
	int n , m , i , j ;
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if (!std_crs1) {cout << "this file 'std_crs1' cannot to open\n" ; exit(0); }
	std_crs1.read((char*) &temp[0] , sizeof(temp[0])) ;
	for ( m=0 , i=0 ; !std_crs1.eof() ; m++ ) {
		cout << "temp.csno :  " << temp[i].csno << endl ;
		cout << "temp.score :  " << temp[i].score << endl ;
		cout << "temp.stdno :  " << temp[i].stdno << endl ;
		i++;
		std_crs1.read((char*) &temp[i] , sizeof(temp[i])) ;
	}
	std_crs1.close() ;
	temp[i]=NULL ;
	cout << "m :   " << m << endl ;
	cout << "enter number to remove student & course :  " ;
	cin >> n ;
	while(1) {
		if (n<=m)
			break ;
		cout << "this number is long !! pls enter another number :   " ;
		cin >> n ;
	}
	ofstream std_crs2("std_crs2.b",ios::binary) ;
	if (!std_crs2) {cout << "this file 'std_crs2' cannot to open\n" ; exit(0); }
	for (j=0 ; n>0 ; ) {
		cout << "enter the student number :  " ;
		cin >> temp2[j].stdno ;
		cout << "enter the course number :  " ;
		cin >> temp2[j].csno ;
		for ( i=0 ; temp[i].csno ; i++) {
			if (temp2[j]==temp[i]) {
				n-- ;j++;
				cout << "this is turest ^*^ \n" ;
			}	
		}
	}
	temp2[j]=NULL ;
	for ( j=0 , i=0 ; temp[i].csno ; i++) {
		cout << "rrr\n" ;
		for (j=0 ; temp2[j].csno ; j++ ) {
			if (temp2[j]==temp[i])
				break ;
		}
		if (temp2[j]==temp[i])
			continue ;
		std_crs2.write((char*)&temp[i] , sizeof(temp[i]));
	}
	std_crs2.close() ;
	
	ifstream std_crs3("std_crs2.b",ios::binary) ;
	if (!std_crs3) {cout << "this file 'std_crs3' cannot to open" ; exit(0); }
	std_crs3.read((char*)&temp3 , sizeof(temp3));
	for (; !std_crs3.eof() ; ) {
		cout << "temp3.csno :  " << temp3.csno << endl ;
		cout << "temp3.score :  " << temp3.score << endl ;
		cout << "temp3.stdno :  " << temp3.stdno << endl ;
		std_crs3.read((char*)&temp3 , sizeof(temp3)); 
	}
	std_crs3.close() ;
	delete("std_crs.b") ;
	rename ("std_crs2.b","std_crs.b") ;
	
	
}


void course::disp_stds_crs (void) {
	student temp1 ;
	std_crs temp ;
	
	ifstream std1("student.b",ios::binary) ;
	if(!std1){cout << "this file 'std1' cannot to open !!!\n" ;	exit(0) ;}
	int csno1 , i=0 ;
	char stdno1[200][15] ;
	
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if(!std_crs1){cout << "this file 'std_crs1' cannot to open !!!\n" ;	exit(0) ;}
	cout << "enter course number:  " ;
	cin >> csno1 ;
	std_crs1.read((char*) &temp , sizeof(temp)) ;
	for (i=0 ; !std_crs1.eof() ; ) {
		if (csno1==temp.csno) {
			strc(stdno1[i] , temp.stdno) ;
			i++ ;
		}
		std_crs1.read((char*) &temp , sizeof(temp)) ;
	}
	if (i==0) {
		cout << "This course does not have a student \n" ;
	}
	std_crs1.close() ;	
	if (i!=0) {
		stdno1[i][0]= ' ' ;
		std1.read((char*)&temp1 , sizeof(temp1)) ;
		for (; !std1.eof() ; ) {
			for (i=0 ; stdno1[i][0] != ' ' ; i++) {
				if (mo(stdno1[i] , temp1.stdno )) {
					cout << temp1 ;
					cout << "course score :  " << temp.score << endl ;
					cout << "-----------------------\n" ;
				}
			}
			std1.read((char*)&temp1 , sizeof(temp1)) ;
		}
		std1.close() ;
	}
}


void student::disp_crss_std (void) {
	course temp1 ;
	std_crs temp ;
	char stdno1[15] ;
	int csno[40] , i=0 ;
	ifstream crs1("course.b",ios::binary) ;
	if(!crs1){cout << "this file 'crs1' cannot to open !!!\n" ;	exit(0) ;}
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if(!std_crs1){cout << "this file 'std_crs1' cannot to open !!!\n" ;	exit(0) ;}
	cout << "enter student number :  " ;
	cin >> stdno1 ;
	std_crs1.read((char*) &temp , sizeof(temp)) ;
	for (i=0 ; !std_crs1.eof() ; ) {
		if (mo(stdno1 , temp.stdno)) {
			csno[i] = temp.csno ;
			i++ ;
		}
		std_crs1.read((char*) &temp , sizeof(temp)) ;
	}
	if (i==0) {
		cout << "This student has no course!!!\n" ;
	}
	std_crs1.close() ;
	if (i!=0) {
		csno[i] = NULL ;
		crs1.read((char*)&temp1 , sizeof(temp1)) ;
		for (; !crs1.eof() ; ) {
			for (i=0 ; csno[i] ; i++) {
				if (csno[i]==temp1.csno) {
					cout << temp1 ;
				}
			}
			crs1.read((char*)&temp1 , sizeof(temp1)) ;
		}
		crs1.close() ;
	}
	
}


void teacher::disp_crss_tch(void) {
	std_crs temp ;
	course temp1 ;
	int tchno , csno[40] ;
	
	int i ;
	
	ifstream std_crs1 ("std_crs.b",ios::binary) ;
	if (!std_crs1) {cout << "this file 'std_crs.b' cannot open !!!"; exit(0);}
	ifstream crs1 ("course.b",ios::binary) ;
	if (!crs1) {cout << "this file 'course.b' cannot open !!!"; exit(0);}
	
	cout << "enter teacher number :  " ;
	cin >> tchno ;
	cout << "****\n" ;
	std_crs1.read((char*)&temp , sizeof(temp));
	for (i=0 ; !std_crs1.eof() ; ) {
		if (temp.tchno==tchno) {
			csno[i] = temp.csno ;
			i++;
		}
		std_crs1.read((char*)&temp , sizeof(temp));
	}
	csno[i] = NULL ;
	std_crs1.close() ;
	crs1.read((char*)&temp1 , sizeof(temp1));
	for ( ; !crs1.eof() ;) {
		for (i=0 ; csno[i] ;i++) {
			if (csno[i]==temp1.csno)
				cout << temp1 ;
		}
		crs1.read((char*)&temp1 , sizeof(temp1));
	}
	crs1.close() ;
	
	
	
	
	
}


void teacher::avg_crss_tch (void) {
	std_crs temp[2000] ;
	int tchno1 , i=0 , m=1 ;
	double sum = 0 ;
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if (!std_crs1) {cout << "this file cannot to open!!!\n"; exit(0);}
	
	std_crs1.read((char*)&temp[0] , sizeof(temp[0]));
	for ( i=1 ; !std_crs1.eof() ; i++)
		std_crs1.read((char*)&temp[i] , sizeof(temp[i]));
	temp[i].csno=NULL ;
	
	do{
		cout << "enter teacher number :  " ;
		cin >> tchno1 ;
		for (i=0 ; temp[i].csno ; i++) {
			if (temp[i].tchno==tchno1) {
				sum=(sum+temp[i].score)/m ;
				m++ ;
			}
		}
		if (m==1) {
			cout << "this teacher number not find !!! pls try again\n" ;
		}else {
			cout << "avarge is :  " << sum ;
		}		
	}while(m==1);
}


void teacher::disp_stds_tch (void) {
	
	std_crs temp[2000] ;
	student temp1 ;
	int i , tch1 , j=0 ;
	char stdno1[200][15] ;
	
	ifstream std_crs1("std_crs.b" , ios::binary ) ;
	if (!std_crs1) {cout << "this file cannot to open!!!\n"; exit(0);}
	ifstream std1("student.b" , ios::binary ) ;
	if (!std1) {cout << "this file cannot to open!!!\n"; exit(0);}
	
	std_crs1.read((char*) &temp[0] , sizeof(temp[0]));
	for (i=1 ; !std_crs1.eof() ; i++)
		std_crs1.read((char*) &temp[i] , sizeof(temp[i]));
	temp[i].csno = NULL ;
	
	
	do {
		cout << "enter teacher number :  " ;
		cin >> tch1 ;
		for (i=0 , j=0 ; temp[i].csno ; i++) {
			if (tch1==temp[i].tchno) {
				strc (stdno1[j] , temp[i].stdno) ;
				j++ ;
			}
		}
		if (j==0) {
			cout << "this tacher not find !! pls try again\n" ;
		}
		
	}while(j==0) ;
	stdno1[j][0] = ' ' ;
	
	std1.read((char*) &temp1 , sizeof(temp1));
	for ( ; !std1.eof() ;) {
		for (j=0 ; stdno1[j][0] != ' ' ;j++) {
			if (mo(temp1.stdno , stdno1[j])) {
				cout << temp1 ;
			}
		}
		std1.read((char*) &temp1 , sizeof(temp1));
	}
}


void teacher::avg_crs_tch (void) {
	std_crs temp , temp1[2000] ;
	int i=0 , m=1 ;
	double sum=0 ;
	ifstream std_crs1("std_crs.b",ios::binary) ;
	if (!std_crs1) {cout << "this file cannot to open!!!\n"; exit(0) ;}
	
	std_crs1.read((char*)& temp1[0] , sizeof(temp1[0])) ;
	for ( i=1 ; !std_crs1.eof() ; i++ )
		std_crs1.read((char*)& temp1[i] , sizeof(temp1[i])) ;
	temp1[i].csno = NULL ;
	std_crs1.close() ;
	
	do {
		cout << "enter course number :  " ;
		cin >> temp.csno ;
		cout << "enter teacher number :  " ;
		cin >> temp.tchno ;
		m=1;
		for (i=0;temp1[i].csno;i++) {
			if (temp1[i].csno==temp.csno && temp1[i].tchno==temp.tchno) {
				sum=(sum+temp1[i].score)/m;
				m++ ;
			}
		}
		if (m==1) {
			cout << "this teacher and course cannot find!!! pls try agin\n" ;
		}else {
			cout << "avrage is :  " << sum << endl ;
		}	
	}while(m==1) ;	
}


void student::dis_avg_std (void) {
	std_crs temp ;
	char stdno1[15] ;
	double sum=0 ;
	int i=1 ;
	ifstream std_crs1 ("std_crs.b",ios::binary) ;
	if(!std_crs1) {cout << "this file cannot to open!!! \n"; exit(0);}
	cout << "enter student number :  " ;
	cin >> stdno1 ;
	std_crs1.read((char*) &temp , sizeof(temp)) ;
	for ( ; !std_crs1.eof() ;) {
		if (mo(stdno1 , temp.stdno)) {
			sum = (sum+temp.score)/i ;
			i++ ;
		}
		std_crs1.read((char*) &temp , sizeof(temp)) ;
	}
	
	if (i==1) {
		cout << "this student not find !!!!\n" ;
	}else {
		cout << "avrage is :   " << sum << endl ;
	}
	
	
}


void student::disp_tchs_std (void) {
	std_crs temp ;
	teacher temp2 ;
	char stdno1[15] ;
	int tchno1[20] , i , j ;	
	ifstream std_crs1("std_crs.b" , ios::binary) ;
	if(!std_crs1){cout << "this file 'std_crs1' cannot open!!!\n"; exit(0);}
	ifstream tch1("teacher.b" , ios::binary) ;
	if(!tch1){cout << "this file 'tch1' cannot open!!!\n"; exit(0);}
	
	cout << "enter student number :   " ;
	cin >> stdno1 ;
	cout << "***\n" ;
	std_crs1.read((char*) &temp , sizeof(temp)) ;
	for ( i=0 ; !std_crs1.eof() ; ) {
		if (mo(stdno1 , temp.stdno)) {
			tchno1[i] = temp.tchno ;
			i++ ;
		}
		std_crs1.read((char*) &temp , sizeof(temp)) ;
	}
	if (i==0)
		cout << "This student has no teacher!!!\n" ;
	if (i!=0) {
		tchno1[i]=NULL ;
		std_crs1.close() ;
		tch1.read((char*) &temp2 , sizeof(temp2)) ;
		for ( ; !tch1.eof() ;) {
			for (i=0 ; tchno1[i] ;i++) {
				if (tchno1[i]==temp2.tchno) {
					cout << temp2 ;
				}
			}
			tch1.read((char*) &temp2 , sizeof(temp2)) ;
		}
	}
	tch1.close() ;	
}


void strc (char b[15] , const char a[15]) {
	for (int i=0 ; a[i] ; i++) {
		b[i] = a[i] ;
	}	
}


bool mo(const char a[15] , const char b[15]) {
	int an=0 , bn=0 , i ;
	for (i=0 ; a[i] ; i++) an++ ;
	for (i=0 ; b[i] ; i++) bn++ ;
	if (an==bn) {
		for ( i=0 ; an>0 ; an-- , i++) {
			if (a[i]!=b[i])
				return false ;
		}
		return true ;
	}
	return false ;
}

















